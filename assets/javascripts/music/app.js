/*jshint globalstrict: true */
/*global angular: true */

'use strict';

/**
 * app
 */
var app = angular.module('app', ["google-maps"]);

/**
 * venue hash lookup
 */
app.value('venueMappings', {
  // concerts
  '/cork-opera-house': 'Cork Opera House',
  '/the-everyman': 'The Everyman',
  '/triskel-christchurch': 'Triskel Christchurch',
  '/triskel-auditorium': 'Triskel Auditorium',
  // music xtra
  '/festival-club-at-the-metropole': 'Festival Club at the Metropole',
  '/old-oak': 'Old Oak',
  '/cyprus-avenue': 'Cyprus Avenue',
  '/the-oliver-plunkett': 'The Oliver Plunkett',
  '/bodega': 'Bodega',
  '/crane-lane-theatre': 'Crane Lane Theatre',
  '/imperial-hotel': 'Imperial Hotel',
  '/the-pavilion': 'The Pavilion',
  '/the-woodford': 'The Woodford',
  '/an-brog': 'An Brog',
  '/reardens': 'Reardens'
});


/**
 * Load partials into template cache
 */
app.run(function($templateCache) {

  // gig list
  $templateCache.put('gig_list.html', [
    '<div class="gig-list-element artists four columns" rm-gig-list-element id="{{ $index }}" ng-repeat="gig in gigsFiltered = (gigs | filter:filter)" ng-class="{last:lastInRow($index, gigsFiltered.length)}">',
    '    <div class="gig-container">',
    '        <div class="image-wrap">',
    '            <div class="circle-image">',
    '                <img ng-src="{{ gig.artists.main.image }}" style="max-width:100%" alt="{{ gig.artists.main.name }}">',
    '            </div>',
    '            <div class="sold-out-circle" ng-show="gig.sold_out"></div>',
    '            <div class="sold-out-circle-text" ng-show="gig.sold_out"><h1>sold out</h1></div>',
    '        </div>',
    '        <div ng-show="gig.is_double_bill" class="double-bill">Double Bill</div>',
    '    </div>',
    '    <div class="copy-container">',
    '        <div class="equalheight">',
    '            <h3 class="artist-hover" style="min-height:55px;">{{ gig.artists.main.name }}</h3>',
    '            <h6 ng-show="gig.is_double_bill" style="min-height:25px;" class="db-alert">Double Bill with: <span>{{ gig.artists.secondary.name }}</span></h6>',
    '        </div>',
    '        <h6 style="margin:0;" ><span>{{ gig.day }} {{ gig.time }}</span><br> @ {{ gig.venue }}</h6>',
    '    </div>',
    '</div>',
    '<div class="no-gig-alert" ng-show="gigsFiltered.length === 0">',
    '    <h4>We could find no featured artists for the chosen filter settings.</h4>',
    '    <button class="sub-filter-btn" ng-click="$parent.resetState(null)">Reset</button>',
    '</div>',
    '<div class="twelve columns centered loading-message"><h4 ng-hide="gigs">Loading concert information</h4><div>'
  ].join("\n"));

  // detail
  $templateCache.put('gig_list_detail.html', [
    '   <div id="gig-list-detail" class="twelve columns gig-list-detail" style="padding-left: 0; padding-right: 0; padding-top: 0;">',
    '       <div class="twelve columns show-for-small" style="margin-bottom:4px;">',
    '           <a class="close-btn float-right" ng-click="close()">x</a>',
    '           <h2>{{ gig.artists.main.name }}</h2>',
    '       </div>',
    '       <div class="six columns hide-for-small concerts-main-youtube video-container">',
    '       <iframe style="min-height:365px" src="//www.youtube.com/embed/{{ gig.artists.main.video }}?HD=1;rel=0;showinfo=0;controls=0" frameborder="0" allowfullscreen ></iframe>',
    '       </div>',
    '       <div class="six columns show-for-small video-container">',
    '       <iframe width="220" src="//www.youtube.com/embed/{{ gig.artists.main.video }}?HD=1;rel=0;showinfo=0;controls=0" frameborder="0" allowfullscreen ></iframe>',
    '       </div>',
    '       <div class="six columns">',
    '           <a class="close-btn float-right hide-for-small" ng-click="close()">x</a>',
    '           <h2 class="hide-for-small">{{ gig.artists.main.name }}</h2>',
    '           <h4 class="lineup">{{ gig.artists.main.lineup }}</h4>',
    '           <h6 class="concert-day-venue-gig"><span>{{ gig.day }} {{ gig.time }}</span> @ {{ gig.venue }}</h6>',
    '           <h6 style="font-size:12px;">{{gig.extra_gig}}</h6>',
    '           <div class="min-bio"><p ng-bind-html-unsafe="gig.artists.main.bio"></p></div>',
    '           <a class="button sub-filter-btn more-info" href="{{ gig.artists.main.url }}">more artist info</a>',
    '       </div>',
    '       <div class="twelve columns">',
    '           <div class="ten columns centered hide-for-small">',
    '               <a class="redButton filter-buy-tickets" target="_blank" ng-show="gig.url" href="{{ gig.url }}" style="width:100%">Buy Tickets</a>',
    '           </div>',
    '           <div class="twelve columns zero show-for-small">',
    '               <a class="redButton filter-buy-tickets" target="_blank" ng-show="gig.url" href="{{ gig.url }}" style="width:100%">Buy Tickets</a>',
    '           </div>',
    '       </div>',
    '   </div>'
  ].join("\n"));

  // schedule
  $templateCache.put('gig_schedule.html', [
    '<p class="pub-artist" ng-show="info" ng-bind-html-unsafe="info"></p>',
    '<div ng-repeat="(subvenue, schedule) in data">',
    '    <div class="twelve columns zero">',
    '       <h2 class="sub-venue-title"  ng-show="subvenue">{{ subvenue }}</h2>',
    '    </div>',
    '    <div ng-show="schedule[\'Thursday 24th\']">',
    '         <div class="twelve columns zero">',
    '             <h3 class="pub-date">Thursday 24th</h3>',
    '             <hr class="hr-sched">',
    '         </div>',
    '         <div class="twelve columns zero">',
    '            <div class="row" ng-repeat="gig in schedule[\'Thursday 24th\']">',
    '                <div class="five mobile-one columns offset one trail-gig pub-time">{{ gig.time }}</div>',
    '                <div class="five mobile-two columns trail-gig pub-artist">{{ gig.name }}</div>',
    '                <div class="two mobile-one columns trail-gig pub-artist">{{ gig.price }} </div>',
    '            </div>',
    '        </div>',
    '    </div>',
    '    <div ng-show="schedule[\'Friday 25th\']">',
    '         <div class="twelve columns zero">',
    '             <h3 class="pub-date">Friday 25th</h3>',
    '             <hr class="hr-sched">',
    '         </div>',
    '         <div class="twelve columns zero">',
    '            <div class="row" ng-repeat="gig in schedule[\'Friday 25th\']">',
    '                <div class="five mobile-one columns offset one trail-gig pub-time">{{ gig.time }}</div>',
    '                <div class="five mobile-two columns trail-gig pub-artist">{{ gig.name }}</div>',
    '                <div class="two mobile-one columns trail-gig pub-artist">{{ gig.price }} </div>',
    '            </div>',
    '        </div>',
    '    </div>',
    '    <div ng-show="schedule[\'Saturday 26th\']">',
    '         <div class="twelve columns zero">',
    '             <h3 class="pub-date">Saturday 26th</h3>',
    '             <hr class="hr-sched">',
    '         </div>',
    '         <div class="twelve columns zero">',
    '            <div class="row" ng-repeat="gig in schedule[\'Saturday 26th\']">',
    '                <div class="five mobile-one columns offset one trail-gig pub-time">{{ gig.time }}</div>',
    '                <div class="five mobile-two columns trail-gig pub-artist">{{ gig.name }}</div>',
    '                <div class="two mobile-one columns trail-gig pub-artist">{{ gig.price }} </div>',
    '            </div>',
    '        </div>',
    '    </div>',
    '    <div ng-show="schedule[\'Sunday 27th\']">',
    '         <div class="twelve columns zero">',
    '             <h3 class="pub-date">Sunday 27th</h3>',
    '             <hr class="hr-sched">',
    '         </div>',
    '         <div class="twelve columns zero">',
    '            <div class="row" ng-repeat="gig in schedule[\'Sunday 27th\']">',
    '                <div class="five mobile-one columns offset one trail-gig pub-time">{{ gig.time }}</div>',
    '                <div class="five mobile-two columns trail-gig pub-artist">{{ gig.name }}</div>',
    '                <div class="two mobile-one columns trail-gig pub-artist">{{ gig.price }} </div>',
    '            </div>',
    '        </div>',
    '    </div>',
    '    <div ng-show="schedule[\'Monday 28th\']">',
    '         <div class="twelve columns zero">',
    '             <h3 class="pub-date">Monday 28th</h3>',
    '             <hr class="hr-sched">',
    '         </div>',
    '         <div class="twelve columns zero">',
    '            <div class="row" ng-repeat="gig in schedule[\'Monday 28th\']">',
    '                <div class="five mobile-one columns offset one trail-gig pub-time">{{ gig.time }}</div>',
    '                <div class="five mobile-two columns trail-gig pub-artist">{{ gig.name }}</div>',
    '                <div class="two mobile-one columns trail-gig pub-artist">{{ gig.price }} </div>',
    '            </div>',
    '        </div>',
    '    </div>',
    '</div>'
  ].join("\n"));
});

/**
 * gig list
 */
app.directive('rmGigList', function($http, $window) {
  return {
    restrict: 'A',
    templateUrl: 'gig_list.html',
    scope: {
      filter: '='
    },
    link: function(scope, el, attrs) {
      // get gig json
      $http.get(attrs.ajax, { cache: true })
        .success(function(json) {
          scope.gigs = json;
        });

      // calculate last element in each row
      scope.lastInRow = function(i, len) {
        // some hackery for mobile...
        var width = angular.element($window).width(),
            columns = (width > 767) ? 3 : 1;
        return ((i + 1) % columns === 0 || i === (len - 1));
      };

      // detach any open gig detail
      // when changing filter criteria
      scope.$watch('filter', function(f) {
        el.find('#gig-list-detail').detach();
      });
    }
  };
});

/**
 * gig schedule
 */
app.directive('rmGigSchedule', function() {
  return {
    restrict: 'A',
    templateUrl: 'gig_schedule.html',
    scope: {
      data: '=',
      info: '='
    }
  };
});

/**
 * gig list element
 */
app.directive('rmGigListElement', function($compile, $location, $anchorScroll) {

  return function(scope, el) {
    var element,
        detail,
        insert;

    element = angular.element(document.createElement('rm-gig-list-detail'));
    detail  = $compile(element)(scope);

    el.bind('click', function() {


      // attach node
      angular.element('.gig-list-detail').detach();
      insert = el.hasClass('last') ? el : el.nextAll('.last:first');
      insert.after(detail);
      // jump to anchor
      $location.hash('gig-list-detail');
      $anchorScroll();
    });
  };
});

/**
 * gig list detail
 */
app.directive('rmGigListDetail', function() {
  return {
    restrict: 'E',
    templateUrl: 'gig_list_detail.html',
    replace: true,
    link: function(scope, el) {
      scope.close = function() {
        angular.element('.gig-list-detail').detach();
      };
    }
  };
});

/**
 * filter controller
 */
var FilterCtrl = function($scope, $http, $timeout, $log, venueMappings, $location) {

  var venues;

  // path lookup
  $scope.showVenue = venueMappings[$location.path()] || false;
  $scope.loadingVenue = !!$scope.showVenue;

  // get venue json
  $http.get('/concerts/json-venues',{ cache: true })
    .success(function(json) {
      venues = json;

      // show a venue page
      if (false !== $scope.showVenue) {
        $scope.resetState('venueOpts');
        $scope.filter = { venue: $scope.showVenue };
        $scope.loadingVenue = false;
      }

    });

  // reset some vars
  $scope.resetState = function(filterOpts) {
    $scope.filter = {};
    $scope.filterOpts = filterOpts;
    $scope.tab = null;
  };

  // filter var watcher

  $scope.$watch('filter.venue', function(name) {

    if (undefined === name || undefined === venues[name]) {
      return;
    }

    // update route... hacky
    $location.path(name.trim().toLowerCase().split(' ').join('-'));

    angular.extend($scope, {
      center: {
        latitude:  venues[name].map.latitude,   // initial map center latitude
        longitude: venues[name].map.longitude  // initial map center longitude
      },
      markers: [ {
        latitude:  venues[name].map.latitude,
        longitude: venues[name].map.longitude
      }],
      zoom: 17      // the zoom level
    });

    $scope.venue = venues[name];
  });

  angular.extend($scope, {
    center: {
      latitude: 51, // initial map center latitude
      longitude: -8 // initial map center longitude
    },
    zoom: 8 // the zoom level
  });
};




