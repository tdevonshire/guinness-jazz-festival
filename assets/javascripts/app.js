(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  $(document).ready(function() {
    $.fn.foundationAlerts           ? $doc.foundationAlerts() : null;
    $.fn.foundationButtons          ? $doc.foundationButtons() : null;
    $.fn.foundationAccordion        ? $doc.foundationAccordion() : null;
    $.fn.foundationNavigation       ? $doc.foundationNavigation() : null;
    $.fn.foundationTopBar           ? $doc.foundationTopBar() : null;
    $.fn.foundationCustomForms      ? $doc.foundationCustomForms() : null;
    $.fn.foundationMediaQueryViewer ? $doc.foundationMediaQueryViewer() : null;
    $.fn.foundationTabs             ? $doc.foundationTabs({callback : $.foundation.customForms.appendCustomMarkup}) : null;
    $.fn.foundationTooltips         ? $doc.foundationTooltips() : null;
    $.fn.foundationMagellan         ? $doc.foundationMagellan() : null;
    $.fn.foundationClearing         ? $doc.foundationClearing() : null;
    $.fn.placeholder                ? $('input, textarea').placeholder() : null;
  });

  // Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if (Modernizr && Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

  /**
   * resize video
   */
  





  /**
   * sliders
   */
  var sliderHome = $("#slider"),
      sliderSponsor = $("#slider-sponsor");

  if (sliderHome.length) {
    sliderHome.responsiveSlides({
      pager: true,
      nav: true,
      maxwidth: 1600,
      speed:1000,
      namespace: "large-btns"
    });
  }

  if (sliderSponsor.length) {
    sliderSponsor.responsiveSlides({
      auto: true,
      pager: false,
      nav: false,
      maxwidth: 1600,
      speed:1500,
      namespace: "sponsers"
    });
  }

  /**
   *use below to embed google map
   */
  $('.maps-reveal').on('click', function() {
    $(this).toggleClass('embed-open');
    $(this).next('.map-revealable').slideToggle(function() {
      var $el = $(this).children('.map-canvas:first-child');
      initialize($el);
    });
  });

  /**
   * below is for the accom page, the reveal is different for this template compared to the guinness pub trail.
   */
  $('.accom-map-reveal').on('click', function() {
    $(this).toggleClass('open');
    var $textForChange = $(this).prev().children(1);
    $(this).prev().toggleClass('red-bg-trans');
    $(this).next('.accom-map-revealable').slideToggle(function() {
      $textForChange.children('h3').toggleClass('white-change');
      var $el = $(this).children('.map-canvas:first-child');
      initialize($el);
    });
  });

  /**
   * basic reveal for content non-map releated
   */
  $('.reveal').on('click', function() {
    var $this = $(this);
    $(this).prev().toggleClass('red-bg-trans');
    $this.toggleClass('open');
    $this.next('.revealable').slideToggle();
  });

  $('.reveal-no-bg').on('click', function() {
    var $this = $(this);
    $this.toggleClass('open');
    $this.next('.revealable-no-bg').slideToggle();
  });

  function initialize($el) {

    var lat  = $el.data('lat'),
        lng  = $el.data('lng'),
        zoom = 17;

    var mapOptions = {
      center: new google.maps.LatLng(lat, lng),
      zoom: zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map($el[0], mapOptions);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng($el.data('lat'), $el.data('lng')),
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        strokeColor: "#b5172c",
        scale: 8
      },
      draggable: false,
      map: map
    });

    var styles = [{
      stylers: [
        { saturation: -100 }]},
        { featureType: "road.arterial",
          elementType: "geometry",
          stylers: [
            { lightness: 100 },
            { visibility: "simplified" }]},
              { featureType: "road.highway",
                elementType: "geometry",
                stylers: [
                  { visibility: "simplified" }]},
                  { featureType: "road",
                    elementType: "labels",
              stylers: [
                  { visibility: "off" }]}];
          map.setOptions({styles: styles});
        }

        /*
        code below floats teh venues for the music-trail page
        http://www.jtricks.com/javascript/navigation/floating.html
        for more info on variables
        */

        var $menu = $('#floatMenu');

        if (undefined !== $menu.addFloating) {
            $('#floatMenu').addFloating({
                targetRight: 0,
                targetLeft: 0,
                targetTop: 10,
                snap: true
            });
        }

  /*
  code below hides the lasst entry in the venues category
  this category is used for structure and doesnt have any
  content to display relating to a venue
  */
  $('#slide_div10').hide();

  /*
  code below is used to equally size elements
  https://github.com/ginader/syncHeight for more information.
  the class .equalheight can be attatched to an elelment thats looped and
  all will be equal height.
  */
  $('.equalheight').syncHeight({updateOnResize: true});
  // unsync they're heights again when the Browser window is narrower than 500px
  $(window).resize(function(){
    if($(window).width() < 500) {
      $('.equalheight').unSyncHeight();
    }
  });

  var footer = $("#footer");
  var pos = footer.position();
  var height = $(window).height();

  height = height - pos.top;
  height = height - footer.height();

  if (height > 0) {
    footer.css({'margin-top' : height+'px'});
  }

  /* nav js */
  var ww = document.body.clientWidth;

  $(document).ready(function() {
    $(".nav li a").each(function() {
      if ($(this).next().length > 0) {
        $(this).addClass("parent");
      }
    });

    $(".toggleMenu").click(function(e) {
      e.preventDefault();
      $(this).toggleClass("active");
      $(".nav").toggle();
    });
    adjustMenu();
  });

  $(window).bind('resize orientationchange', function() {
    ww = document.body.clientWidth;
    adjustMenu();
  });

  var adjustMenu = function() {
    if (ww < 768) {
      $(".toggleMenu").css("display", "inline-block");
      if (!$(".toggleMenu").hasClass("active")) {
        $(".nav").hide();
      } else {
        $(".nav").show();
      }
      $(".nav li").unbind('mouseenter mouseleave');
      $(".nav li a.parent").unbind('click').bind('click', function(e) {
        // must be attached to anchor element to prevent bubbling
        e.preventDefault();
        $(this).parent("li").toggleClass("hover");
      });
    } else if (ww >= 768) {
      $(".toggleMenu").css("display", "none");
      $(".nav").show();
      $(".nav li").removeClass("hover");
      $(".nav li a").unbind('click');
      $(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
        // must be attached to li so that mouseleave is not triggered when hover over submenu
        $(this).toggleClass('hover');
      });
    }
  };



  /**
   * Ads
   */

  var mpus = [
    {
      src: '/assets/images/ads/Billy&Port.jpg',
      href: '/concerts/detail/everyman-listings/billy-spectrum-portico#billy-cobham-spectrum-40',
      onclick: "_gaq.push(['_trackEvent', 'billy-spectrum-portico', clicked'])"
    },
    {
      src: '/assets/images/ads/Chic.jpg',
      href: '/concerts/detail/opera-house-listings/chic-nile#chic-nile-rodgers',
      onclick: "_gaq.push(['_trackEvent', 'chic-nile', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Court&Rene.jpg',
      href: '/concerts/detail/everyman-listings/courtney-pine-rene-marie#courtney-pine',
      onclick: "_gaq.push(['_trackEvent', 'courtney-pine-rene-marie', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Saluzzi&Perico.jpg',
      href: '/concerts/detail/triskel-christchurch-listings/saluzzi-lechner-saluzzi-trio-perico-sambeat#saluzzi-lechner-saluzzi-trio',
      onclick: "_gaq.push(['_trackEvent', 'saluzzi-lechner-saluzzi-trio-perico-sambeat', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Efterklang.jpg',
      href: '/concerts/detail/opera-house-listings/efterklang#efterklang',
      onclick: "_gaq.push(['_trackEvent', 'efterklang', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    },
    {
      src: '/assets/images/ads/Soul&Bilial.jpg',
      href: '/concerts/detail/opera-house-listings/soul-ii-soul-bilal#soul-ii-soul',
      onclick: "_gaq.push(['_trackEvent', 'soul-ii-soul-bilal', 'clicked'])"
    }
  ];

  var idx = Math.floor(Math.random() * mpus.length);
  var mpu = mpus[idx];

  // create img
  var img = $('<img />', {
    src: mpu.src
  });

  // create link
  var a = $('<a></a>', {
    href: mpu.href,
    html: img,
   onclick: mpu.onclick
  });

  // append to #mpu
  // note... make sure #mpu element is empty
  a.appendTo('#mpu');

})(jQuery, this);