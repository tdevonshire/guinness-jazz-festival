<?php

/*
=====================================================
 This ExpressionEngine add-on was created by Laisvunas
 - http://devot-ee.com/developers/laisvunas
=====================================================
 Copyright (c) Laisvunas
=====================================================
 This is commercial Software.
 One purchased license permits the use this Software on the SINGLE website.
 Unless you have been granted prior, written consent from Laisvunas, you may not:
 * Reproduce, distribute, or transfer the Software, or portions thereof, to any third party
 * Sell, rent, lease, assign, or sublet the Software or portions thereof
 * Grant rights to any other person
=====================================================
 Purpose: Allows you to display content from multiple pages using Ajax. Any ExpressionEngine tag outputting pagination links is supported!
=====================================================
*/

$plugin_info = array(
						'pi_name'			=> 'AJAX Pagination',
						'pi_version'		=> '1.1',
						'pi_author'			=> 'Laisvunas',
						'pi_author_url'		=> 'http://devot-ee.com/developers/laisvunas',
						'pi_description'	=> 'Allows you to display content from multiple pages using Ajax. Any ExpressionEngine tag outputting pagination links is supported!',
						'pi_usage'			=> Ajax_pagination::usage()
					);

class Ajax_pagination {
  
  // ----------------------------------------
  //  Constructor
  // ----------------------------------------
  
  function Ajax_pagination()
  {
    $this->EE =& get_instance();
  }
  // END FUNCTION
  
  function links()
  {
    // Fetch the tagdata
    $tagdata = trim($this->EE->TMPL->tagdata);
    //echo '$tagdata: ['.$tagdata.']'.PHP_EOL;

    // fetch params
    $pagination_symbol = $this->EE->TMPL->fetch_param('pagination_symbol') ? $this->EE->TMPL->fetch_param('pagination_symbol') : 'P';
    $tag_count = $this->EE->TMPL->fetch_param('tag_count'); // added by exp:ajax_pagination:wrapper tag
    
    // Define variables

    if ($tagdata)
    {
      // add onclick attributes to pagination links
      $tagdata = $this->_change_pagination_links($tagdata, $pagination_symbol, $tag_count);  
    }

    return $tagdata;
  }
  // END FUNCTION
  
  function wrapper()
  {
    // Fetch the tagdata
    $tagdata = $this->EE->TMPL->tagdata;
    //echo '$tagdata: ['.$tagdata.']'.PHP_EOL;
    
    // fetch params
    $embed_template_url = $this->EE->TMPL->fetch_param('embed_template_url');
    $ajax_container = $this->EE->TMPL->fetch_param('ajax_container');
    $process_indicator = $this->EE->TMPL->fetch_param('process_indicator');
    $process_indicator_scroll = $this->EE->TMPL->fetch_param('process_indicator_scroll');
    $ajax_pagination_vars = $this->EE->TMPL->fetch_param('ajax_pagination_vars');
    $ajax_pagination_values = $this->EE->TMPL->fetch_param('ajax_pagination_values');
    
    // param "embed_template_url" is required
    if (!$embed_template_url)
    {
      echo 'ERROR! Parameter "embed_template_url" of exp:ajax_pagination tag must be defined!<br><br>'.PHP_EOL;;
    }
    // param "ajax_container" is required
    if (!$ajax_container)
    {
      echo 'ERROR! Parameter "ajax_container" of exp:ajax_pagination tag must be defined!<br><br>'.PHP_EOL;;
    }
    
    // form embed template URL
    $embed_template_url = $this->_parse_url_in_parameter($embed_template_url);
    
    // is template being loaded by AJAX?
    $is_ajax = ($this->EE->input->post('ajax') == 'yes') ? TRUE : FALSE;
    
    // embed vars received by AJAX call
    if ($is_ajax)
    {
      $ajax_pagination_vars = $this->EE->input->post('ajax_pagination_vars');
      $ajax_pagination_values = $this->EE->input->post('ajax_pagination_values');
      //echo '$ajax_pagination_vars: ['.$ajax_pagination_vars.']<br><br>'.PHP_EOL;
      //echo '$ajax_pagination_values: ['.$ajax_pagination_values.']<br><br>'.PHP_EOL;
    }
    
    // js messages
    $js_messages = array();
    $js_messages['error_encountered_msg'] = 'An error was encountered: ';
    $js_messages['no_response_msg'] = 'No response from server.';
    $js_messages['no_container_msg'] = 'Ajax container not found.';
    
    // find how many exp:ajax_pagination tags there are on the page
    $tag_count = $this->_find_tag_count($ajax_pagination_vars, $ajax_pagination_values);
    
    $tagdata = str_replace(LD.'exp:ajax_pagination:links', LD.'exp:ajax_pagination:links tag_count="'.$tag_count.'" ', $tagdata);
    //echo '$tagdata: ['.$tagdata.']'.PHP_EOL;
    
    // parse embed vars
    $tagdata = $this->_parse_embed_vars($tagdata, $ajax_pagination_vars, $ajax_pagination_values);
    
    // fetch var pair {ajax_pagination_container_top}{/ajax_pagination_container_top}
    $ajax_pagination_container_top = $this->_getVariablePair('ajax_pagination_container_top', $tagdata);
    $ajax_pagination_container_top_tagdata = $ajax_pagination_container_top['value'];
    //echo '$ajax_pagination_container_top_tagdata: ['.$ajax_pagination_container_top_tagdata.']<br><br>'.PHP_EOL;
    
    // fetch var pair {ajax_pagination_container_bottom}{/ajax_pagination_container_bottom}
    $ajax_pagination_container_bottom = $this->_getVariablePair('ajax_pagination_container_bottom', $ajax_pagination_container_top['data_beneath']);
    $ajax_pagination_container_bottom_tagdata = $ajax_pagination_container_bottom["value"];
    //echo '$ajax_pagination_container_bottom_tagdata: ['.$ajax_pagination_container_bottom_tagdata.']<br><br>'.PHP_EOL;
    
    $ajax_tagdata = $ajax_pagination_container_bottom['data_above'];
    //echo '$ajax_tagdata: ['.$ajax_tagdata.']<br><br>'.PHP_EOL;
    
    if ($is_ajax)
    {
      $out = $ajax_tagdata;
    }
    else
    {
      // form javascript
      $js = $this->_js($tag_count, $js_messages, $process_indicator, $process_indicator_scroll, $ajax_pagination_vars, $ajax_pagination_values, $embed_template_url, $ajax_container);
      $out = $js.$ajax_pagination_container_top_tagdata.$ajax_tagdata. $ajax_pagination_container_bottom_tagdata;
    }
    
    return $out;
  }
  // END FUNCTION
  
  function _find_tag_count($ajax_pagination_vars, $ajax_pagination_values)
  {
    $tag_count = 1;
    
    if ($ajax_pagination_vars AND $ajax_pagination_values)
    {
      $ajax_pagination_vars_arr = explode('|', $ajax_pagination_vars);
      $ajax_pagination_values_arr = explode('|', $ajax_pagination_values);
      
      for ($i = 0; $i < count($ajax_pagination_vars_arr); $i++)
      {
        if ($ajax_pagination_vars_arr[$i] == 'tag_count' AND $ajax_pagination_values_arr[$i])
        {
          $tag_count = $ajax_pagination_values_arr[$i];
        }
      }
    }
    
    return $tag_count;
  }
  // END FUNCTION
  
  function _parse_embed_vars($data, $ajax_pagination_vars, $ajax_pagination_values)
  {   
    if ($ajax_pagination_vars AND $ajax_pagination_values)
    {
      
      $ajax_pagination_vars_arr = explode('|', $ajax_pagination_vars);
      $ajax_pagination_values_arr = explode('|', $ajax_pagination_values);
      
      // output embed vars
      $conds = array();
      for ($i = 0; $i < count($ajax_pagination_vars_arr); $i++)
      {
        $var_name = 'ajax_pagination_embed_'.$ajax_pagination_vars_arr[$i];
        $conds[$var_name] = (isset($ajax_pagination_values_arr[$i]) AND $ajax_pagination_values_arr[$i]) ? $ajax_pagination_values_arr[$i] : '';
        $data = $this->EE->TMPL->swap_var_single($var_name, $conds[$var_name], $data);
      }
      // prepare conditionals
      $data = $this->EE->functions->prep_conditionals($data, $conds);
      // clean undefined embed vars
      $pattern =  '/\{ajax_pagination_embed_(.*)\}/Usi';
      $data = preg_replace($pattern, '', $data);
    }
    
    return $data;
  }
  // END FUNCTION
  
  function _change_pagination_links($pagination_links, $pagination_symbol, $tag_count)
  {
    $pattern = '/href=\"(.*)\"/Usi';
    
    while (preg_match($pattern, $pagination_links, $matches))
    {
      $url = trim(@$matches[1], '/');
      //echo '$url: ['.$url.']<br><br>'.PHP_EOL;
      $last_segment = end(explode('/', $url));
      //echo '$last_segment: ['.$last_segment.']<br><br>'.PHP_EOL;
      $pos = strpos($last_segment, $pagination_symbol);
      //echo '$pos: ['.$pos.']<br><br>'.PHP_EOL;
      if ($pos === 0)
      {
        $pagination_segment = $last_segment;
      }
      else
      {
        $pagination_segment = '';
      }
      //echo '$ajax_url: ['.$ajax_url.']<br><br>'.PHP_EOL;
      $onclick_param = 'onclick="ajaxPagination'.$tag_count.'.fetchData(\''.$pagination_segment.'\');"';
      $pagination_links = preg_replace($pattern, $onclick_param, $pagination_links, 1);
    }
    
    return $pagination_links;
  }
  // END FUNCTION
  
  function _getVariablePair($var_name, $data)
  {
    if (strpos($data, LD.$var_name.RD) !== FALSE AND strpos($data, LD.'/'.$var_name.RD) !== FALSE)
    {
      $opening_tag_pos = strpos($data, LD.$var_name.RD);
      //echo '$opening_tag_pos: '.$opening_tag_pos.'<br><br>'.PHP_EOL;
      $var_pair['data_above'] = substr($data, 0, $opening_tag_pos);
      //echo '$var_pair[\'data_above\']: ['.$var_pair['data_above'].']<br><br>'.PHP_EOL;
      $opening_tag_pos += strlen(LD.$var_name.RD);
      //echo '$opening_tag_pos: ['.$opening_tag_pos.']<br><br>'.PHP_EOL;
      $data_bottom = substr($data, $opening_tag_pos);
      //echo '$var_pair[\'data_beneath\']: ['.$var_pair['data_beneath'].']<br><br>'.PHP_EOL;
      $data_bottom_splitted = explode(LD.'/'.$var_name.RD, $data_bottom, 2);
      if (count($data_bottom_splitted) == 2)
      {
        $var_pair['value'] = $data_bottom_splitted[0];
        //echo '$var_pair[\'value\']: ['.$var_pair['value'].']<br><br>'.PHP_EOL;
        $var_pair['data_beneath'] = $data_bottom_splitted[1];
        $var_pair['data_without'] = $var_pair['data_above'].$var_pair['data_beneath'];
        //echo '$var_pair[\'data_without\']: ['.$var_pair['data_without'].']<br><br>'.PHP_EOL;
         
      }
      else
      {
        $var_pair['value'] = '';
        $var_pair['data_without'] = $data;
      }
    }
    else
    {
      $var_pair['value'] = '';
      $var_pair['data_without'] = $data;
      $var_pair['data_above'] = '';
      $var_pair['data_beneath'] = '';
    }
    return $var_pair;
  }
  // END FUNCTION
  
  function _parse_url_in_parameter($parameter_value)
  {
    // parse {site_id}, {site_url}, {site_index}, {homepage}
    $site_id = $this->EE->config->item('site_id');
    $site_url = trim(stripslashes($this->EE->config->item('site_url')), '/');
    $site_index = stripslashes($this->EE->config->item('site_index'));
    $homepage = $site_url.'/'.$site_index;
    
    $parameter_value = str_replace(LD.'site_id'.RD, $site_id, $parameter_value);
    $parameter_value = str_replace(LD.'site_url'.RD, $site_url, $parameter_value);
    $parameter_value = str_replace(LD.'site_index'.RD, $site_index, $parameter_value);
    $parameter_value = str_replace(LD.'homepage'.RD, $homepage, $parameter_value);
    
    $parameter_value = str_replace('&#47;', '/', $parameter_value);
    $parameter_value = trim($parameter_value, '/').'/';
    //echo '$parameter_value: ['.$parameter_value.']<br><br>'.PHP_EOL;
    
    return $parameter_value;
  }
  // END FUNCTION
  
  function _js($tag_count, $js_messages, $process_indicator, $process_indicator_scroll, $ajax_pagination_vars, $ajax_pagination_values, $embed_template_url, $ajax_container)
  {
    ob_start(); 
?>

<script type="text/javascript">
//<![CDATA[

ajaxPagination<?=@$tag_count?> = {

  XHR: null,
  
  embed_template_url: "<?= @$embed_template_url ?>",
  ajax_container_id: "<?= @$ajax_container ?>",
  
  process_indicator: "<?= @$process_indicator ?>",
  process_indicator_scroll: "<?= @$process_indicator_scroll ?>",
  
  // messages
  error_encountered_msg: "<?= @$js_messages['error_encountered_msg'] ?>",
  no_response_msg: "<?= @$js_messages['no_response_msg'] ?>",
  no_container_msg: "<?= @$js_messages['no_container_msg'] ?>",
  
  // embed vars
  ajax_pagination_vars: "<?=@$ajax_pagination_vars?>",
  ajax_pagination_values: "<?=@$ajax_pagination_values?>",
  
  // helper functions
  
  createRequestObject: function() {
    var requestObject;
    
    try {
   		 requestObject = new ActiveXObject("Msxml2.XMLHTTP");
   	}
    catch (e) {
      try {
        requestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch (e2) {
        requestObject = '';
      }
    }
    if(!requestObject && typeof XMLHttpRequest != "undefined") {
      requestObject = new XMLHttpRequest();
    }
    if (!requestObject) {
      alert('This browser does not support AJAX!'); 
      return false;
    }
    else {
      return requestObject;
    }
  },
  
  trim: function(str, charlist) {
    var whitespace, l = 0, i = 0;
    str += "";
    
    if (!charlist) {
        whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    } else {
        charlist += "";
        whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, "\$1");
    }
    
    l = str.length;
    for (i = 0; i < l; i++) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(i);
            break;
        }
    }
    
    l = str.length;
    for (i = l - 1; i >= 0; i--) {
        if (whitespace.indexOf(str.charAt(i)) === -1) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    
    return whitespace.indexOf(str.charAt(0)) === -1 ? str : "";
  },
  
  getElementsByClass: function(className, tag, elm) {	
    if (document.getElementsByClassName) {
   			elm = elm || document;
   			var elements = elm.getElementsByClassName(className),
   				nodeName = (tag)? new RegExp("\\b" + tag + "\\b", "i") : null,
   				returnElements = [],
   				current;
   			for(var i=0, il=elements.length; i<il; i+=1){
   				current = elements[i];
   				if(!nodeName || nodeName.test(current.nodeName)) {
   					returnElements.push(current);
   				}
   			}
   			return returnElements;
   	}
   	else if (document.evaluate) {
   			tag = tag || "*";
   			elm = elm || document;
   			var classes = className.split(" "),
   				classesToCheck = "",
   				xhtmlNamespace = "http://www.w3.org/1999/xhtml",
   				namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace : null,
   				returnElements = [],
   				elements,
   				node;
   			for(var j=0, jl=classes.length; j<jl; j+=1){
   				classesToCheck += "[contains(concat(\' \', @class, \' \'), \' " + classes[j] + " \')]";
   			}
   			try	{
   				elements = document.evaluate(".//" + tag + classesToCheck, elm, namespaceResolver, 0, null);
   			}
   			catch (e) {
   				elements = document.evaluate(".//" + tag + classesToCheck, elm, null, 0, null);
   			}
   			while ((node = elements.iterateNext())) {
   				returnElements.push(node);
   			}
   			return returnElements;
   	}
   	else {
   			tag = tag || "*";
   			elm = elm || document;
   			var classes = className.split(" "),
   				classesToCheck = [],
   				elements = (tag === "*" && elm.all)? elm.all : elm.getElementsByTagName(tag),
   				current,
   				returnElements = [],
   				match;
   			for(var k=0, kl=classes.length; k<kl; k+=1){
   				classesToCheck.push(new RegExp("(^|\\s)" + classes[k] + "(\\s|$)"));
   			}
   			for(var l=0, ll=elements.length; l<ll; l+=1){
   				current = elements[l];
   				match = false;
   				for(var m=0, ml=classesToCheck.length; m<ml; m+=1){
   					match = classesToCheck[m].test(current.className);
   					if (!match) {
   						break;
   					}
   				}
   				if (match) {
   					returnElements.push(current);
   				}
   			}
   			return returnElements;
   	}
  },
  
  hide: function(el) {
    if (!el.getAttribute('displayOld')) {
        el.setAttribute("displayOld", el.style.display);
    }
    el.style.display = "none";
  },
  
  show: function(el) {
    var getRealDisplay;
    var old;
    var nodeName;
    var body;
    var display;
    var testElem;
    
    getRealDisplay = function(elem) {
      if (elem.currentStyle) {
        return elem.currentStyle.display;
      } 
      else if (window.getComputedStyle) {
        var computedStyle = window.getComputedStyle(elem, null);
        return computedStyle.getPropertyValue('display');
      }
    };
    
    if (typeof this.displayCache == 'undefined') {
      this.displayCache = {};
    }
    
    if (getRealDisplay(el) != 'none') return;
    
    old = el.getAttribute("displayOld");
    el.style.display = old || "";
    
    if (getRealDisplay(el) === "none") {
      nodeName = el.nodeName;
      body = document.body;
      if (this.displayCache[nodeName]) {
        display = this.displayCache[nodeName];
      }
      else {
        testElem = document.createElement(nodeName);
        body.appendChild(testElem);
        display = getRealDisplay(testElem);
        if (display === "none") {
          display = "block";
        }
        body.removeChild(testElem);
        this.displayCache[nodeName] = display;
      }
      el.setAttribute('displayOld', display);
      el.style.display = display;
    }
  },
  
  // custom stuff
  
  fetchData: function(pagination_url_segment) {
    var data_string;
    var ajax_container;
    var ajax_url;
    
    //alert('ajaxPagination<?=@$tag_count?>.embed_template_url: ' + ajaxPagination<?=@$tag_count?>.embed_template_url);
    //alert('ajaxPagination<?=@$tag_count?>.ajax_container_id: ' + ajaxPagination<?=@$tag_count?>.ajax_container_id);
    
    ajax_container = document.getElementById(ajaxPagination<?=@$tag_count?>.ajax_container_id);
    if (!ajax_container) {
      alert(ajaxPagination<?=@$tag_count?>.no_container_msg);
      return;
    }
    
    ajax_url = ajaxPagination<?=@$tag_count?>.embed_template_url + '/' + pagination_url_segment;
    
    ajaxPagination<?=@$tag_count?>.start_process_indicator(ajax_container);
    
    if (ajaxPagination<?=@$tag_count?>.XHR) {
      ajaxPagination<?=@$tag_count?>.XHR.abort();
    }
    else {
      ajaxPagination<?=@$tag_count?>.XHR = ajaxPagination<?=@$tag_count?>.createRequestObject();
    }
    
    // put embed vars into data string 
    data_string = 'ajax=yes';
    if (ajaxPagination<?=@$tag_count?>.ajax_pagination_vars && ajaxPagination<?=@$tag_count?>.ajax_pagination_values) {
      data_string += '&ajax_pagination_vars=' + encodeURIComponent(ajaxPagination<?=@$tag_count?>.ajax_pagination_vars) + '&ajax_pagination_values=' + encodeURIComponent(ajaxPagination<?=@$tag_count?>.ajax_pagination_values);
    }
    //alert('data_string: ' + data_string);
    
    ajaxPagination<?=@$tag_count?>.XHR.open("POST", ajax_url, true);
    ajaxPagination<?=@$tag_count?>.XHR.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    ajaxPagination<?=@$tag_count?>.XHR.setRequestHeader("Connection", "close");
    ajaxPagination<?=@$tag_count?>.XHR.send(data_string);
    ajaxPagination<?=@$tag_count?>.XHR.onreadystatechange = function() {
      if (ajaxPagination<?=@$tag_count?>.XHR.readyState == 4) {
        ajaxPagination<?=@$tag_count?>.resultOfFetchData(ajax_container);
      }
    }
  },
  
  resultOfFetchData: function(ajax_container) {
    var response;
    var error_encountered;
    var error_message;
    
    //alert('resultOfFetchData func!');
    
    if (ajaxPagination<?=@$tag_count?>.XHR.status != 200) {
      response = ajaxPagination<?=@$tag_count?>.error_encountered_msg + ajaxPagination<?=@$tag_count?>.XHR.status;
      error_encountered = true;
    }
    
    response = ajaxPagination<?=@$tag_count?>.XHR.responseText;
    //alert('response: [' + response + ']' + ajaxPagination<?=@$tag_count?>.XHR.status);
    
    if (response == 'null' || ajaxPagination<?=@$tag_count?>.trim(response) == '') {
   			response = ajaxPagination<?=@$tag_count?>.no_response_msg;
      error_encountered = true;
  		}
    
    //response = 'No response from server.';
    //error_encountered = true;
    
    ajaxPagination<?=@$tag_count?>.stop_process_indicator(ajax_container);
    
    if (error_encountered != true) {
      ajax_container.innerHTML = response;
    }
    else {
      alert(response);
    }
  },
  
  start_process_indicator: function(ajax_container) {
    var process_indicator;
    
    if (ajaxPagination<?=@$tag_count?>.process_indicator) {
      process_indicator = ajaxPagination<?=@$tag_count?>.getElementsByClass(ajaxPagination<?=@$tag_count?>.process_indicator, '', ajax_container);
      //alert('process_indicator.length: ' + process_indicator.length);
      if (process_indicator.length > 0) {
        for (var i = 0; i < process_indicator.length; i++) {
          //process_indicator[i].style.border = '2px solid red';
          ajaxPagination<?=@$tag_count?>.show(process_indicator[i]);
        }
        if (ajaxPagination<?=@$tag_count?>.process_indicator_scroll == 'yes') {
          process_indicator[0].scrollIntoView(true);
        }
      }
    }
  },
  
  stop_process_indicator: function(ajax_container) {
    var process_indicator;
    
    if (ajaxPagination<?=@$tag_count?>.process_indicator) {
      process_indicator = ajaxPagination<?=@$tag_count?>.getElementsByClass(ajaxPagination<?=@$tag_count?>.process_indicator, '', ajax_container);
      //alert('process_indicator.length: ' + process_indicator.length);
      if (process_indicator.length > 0) {
        for (var i = 0; i < process_indicator.length; i++) {
          ajaxPagination<?=@$tag_count?>.hide(process_indicator[i]);
        }
      }
    }
  }
}

//]]>
</script>

<?php
    $buffer = ob_get_contents();
    ob_end_clean(); 
    
    return $buffer;
  }
  // END FUNCTION
  
  // ----------------------------------------
  //  Plugin Usage
  // ----------------------------------------
  
  function usage()
  {
    ob_start(); 
?>

THE TAG {exp:ajax_pagination:wrapper}

This tag is used to wrap all contents of the embed template (see the examples' code).
It has following parameters:

1) embed_template_url - required. Allows you to specify URL of the embed template. 
You must specify full URL, i.e. starting with "http". This parameter accepts inside its value
the following ExpressionEngine variables: site_id, site_url, site_index, homepage.

2) ajax_container - required. Allows you to specify HTML id parameter of the HTML element
which will act as the container to place data loaded by AJAX.

3) process_indicator - optional. Allows you to specify HTML class parameter of the HTML element
which will act as the indicator of the page being loaded by AJAX.

4) process_indicator_scroll - optional. Allows you to specify if HTML element
which acts as the indicator of the page being loaded by AJAX should be scrolled into view (value "yes") or not (value "no").
Default is "no".

5) ajax_pagination_vars - optional. Should be used when there is a need to output embed variables inside embed template.
Inside main template you should specify embed variable "ajax_pagination_vars" which contains pipe-delimited list of variables, 
e.g. ajax_pagination_vars="channel|category". Then in the embed template you should add parameter ajax_pagination_vars="{embed:ajax_pagination_vars}". 
Thus listed variables then can be used by adding to their names "ajax_pagination_embed_", e.g. {ajax_pagination_embed_channel}, {ajax_pagination_embed_category}
(see complex example's code).


6) ajax_pagination_values - optional. Should be used when there is a need to output embed variables inside embed template. 
Inside main template you should specify embed variable "ajax_pagination_values" which contains pipe-delimited list of values of embed variables, 
e.g. ajax_pagination_values="technical|279". Then in the embed template you should add parameter ajax_pagination_values="{embed:ajax_pagination_values}".
Thus listed variables then can be used by adding to their names "ajax_pagination_embed_", e.g. {ajax_pagination_embed_channel}, {ajax_pagination_embed_category}
 (see complex example's code).

THE TAG {exp:ajax_pagination:links}

This tag is used to wrap all contents inside the variable pair {paginate}{/paginate} (see the examples' code). 
It has following parameter:

1) pagination_symbol - optional. Allows you to specify symbol which precedes pagination number in URL. Default value is "P".

USAGE

I. Simple example (without using embed variables)

Main template (e.g. technical/ajax_pagination):

<html>

<head>

<title>AJAX Pagination demo</title>

<style type="text/css">

/*Common*/

body {
color: black;
font-family: "arial unicode ms", helvetica, arial, sans-serif;
margin: 0;
font-size: small;
padding: 0;
}

a {
color: #4372aa;
text-decoration: none;
cursor: pointer;
}

a:hover {
text-decoration: underline;
}

/*Main content*/

#maincontent {
width: 48em;
position: relative;
left: 50%;
margin-left: -24em;
padding-bottom: 1em;
}

/* Headings */

h1 {
font-size: 1.5em;
font-weight: normal;
}

h2 {
font-size: 1.1em;
font-weight: bold;
}

/* Indicator */

.process_indicator {
background-image: url({homepage}/images/ajax-loader.gif);
background-repeat: no-repeat;
background-position: bottom left;
height: 3em;
width: 100%;
text-align: left;
display: none;
}

</style>

</head>

<body>

<div id="maincontent">

<h1>AJAX Pagination demo</h1>

{embed="technical/embed_ajax_pagination"}

</div>

</body>

</html>

Embed template (e.g. technical/embed_ajax_pagination):

{exp:ajax_pagination:wrapper 
ajax_container="ajax_container" 
embed_template_url="{homepage}/technical/embed_ajax_pagination" 
process_indicator="process_indicator" 
process_indicator_scroll="yes" 
parse="inward"
}

{ajax_pagination_container_top}
<div id="ajax_container">
{/ajax_pagination_container_top}

<div class="process_indicator">
Loading...
</div>

{exp:channel:entries channel="technical" category="279" sort="asc" orderby="title" limit="5" paginate="both" dynamic="off" parse="inward"}

<p>{absolute_count}. <a href="{path="technical/demo/{url_title}"}">{title}</a></p>

{paginate}{exp:ajax_pagination:links}{pagination_links}{/exp:ajax_pagination:links}{/paginate}

{/exp:channel:entries}

{ajax_pagination_container_bottom}
</div>
{/ajax_pagination_container_bottom}

{/exp:ajax_pagination:wrapper}

II. Complex example (embed variables used)

Main template (e.g. technical/ajax_pagination):

<html>

<head>

<title>AJAX Pagination demo</title>

<style type="text/css">

/*Common*/

body {
color: black;
font-family: "arial unicode ms", helvetica, arial, sans-serif;
margin: 0;
font-size: small;
padding: 0;
}

a {
color: #4372aa;
text-decoration: none;
cursor: pointer;
}

a:hover {
text-decoration: underline;
}

/*Main content*/

#maincontent {
width: 48em;
position: relative;
left: 50%;
margin-left: -24em;
padding-bottom: 1em;
}

/* Headings */

h1 {
font-size: 1.5em;
font-weight: normal;
}

h2 {
font-size: 1.1em;
font-weight: bold;
}

/* Indicator */

.process_indicator {
background-image: url({homepage}/images/ajax-loader.gif);
background-repeat: no-repeat;
background-position: bottom left;
height: 3em;
width: 100%;
text-align: left;
display: none;
}

</style>

</head>

<body>

<div id="maincontent">

<h1>AJAX Pagination demo</h1>

{embed="technical/embed_ajax_pagination" ajax_pagination_vars="channel|category" ajax_pagination_values="technical|279"}

</div>

</body>

</html>

Embed template (e.g. technical/embed_ajax_pagination):

{exp:ajax_pagination:wrapper 
ajax_container="ajax_container" 
embed_template_url="{homepage}/technical/embed_ajax_pagination" 
process_indicator="process_indicator" 
process_indicator_scroll="yes" 
ajax_pagination_vars="{embed:ajax_pagination_vars}" 
ajax_pagination_values="{embed:ajax_pagination_values}" parse="inward"}

{ajax_pagination_container_top}
<div id="ajax_container">
{/ajax_pagination_container_top}

<div class="process_indicator">
Loading...
</div>

{exp:channel:entries channel="{ajax_pagination_embed_channel}" category="{ajax_pagination_embed_category}" sort="asc" orderby="title" limit="5" paginate="both" dynamic="off" parse="inward"}

<p>{absolute_count}. <a href="{path="technical/demo/{url_title}"}">{title}</a></p>

{paginate}{exp:ajax_pagination:links}{pagination_links}{/exp:ajax_pagination:links}{/paginate}

{/exp:weblog:entries}

{ajax_pagination_container_bottom}
</div>
{/ajax_pagination_container_bottom}

{/exp:ajax_pagination:wrapper}

III. Displaying several paginated lists in one template

Displaying several paginated lists in one template is easy.
You only need to define additional variable "tag_count" for each embed template. For example:

{embed="technical/embed_ajax_pagination" ajax_pagination_vars="tag_count" ajax_pagination_values="1"}

{embed="technical/embed_ajax_pagination2" ajax_pagination_vars="tag_count" ajax_pagination_values="2"}

{embed="technical/embed_ajax_pagination3" ajax_pagination_vars="tag_count" ajax_pagination_values="3"}

<?php
    $buffer = ob_get_contents();
    	
    ob_end_clean(); 
    
    return $buffer;
  }
  // END FUNCTION
}
// END CLASS
?>