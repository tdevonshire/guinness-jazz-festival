#/bin/bash

echo "sorting permissions"

chmod 777 \
	system/expressionengine/cache/ \
	images/avatars/uploads/ \
	images/captchas/ \
	images/member_photos/ \
	images/pm_attachments/ \
	images/signature_attachments/ \
	images/uploads/

echo "done. exit status $?"